/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./app.vue",
    "./error.vue",
  ],
  theme: {
    extend: {
      colors: {
        'primary': '#F59F6E',
        'secondary': '#EBE0CF',
        'stroke': '#b5b6b7',
        'typo': '#222222',
      },
      fontFamily: {
        heading: ['Poppins', 'sans-serif']
      },
    },
  },
  plugins: [],
}

